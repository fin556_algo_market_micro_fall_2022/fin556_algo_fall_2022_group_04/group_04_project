#!/bin/bash
cd /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/
#git clone git@gitlab.engr.illinois.edu:shared_code/example_trading_strategies/dia_index_arb_strategy.git
cd group_04_project
mkdir -p /home/vagrant/ss/bt/strategies_dlls
make copy_strategy
cd /home/vagrant/ss/bt/ ; ./StrategyServerBacktesting &
sleep 1
cd /home/vagrant/ss/bt/utilities
echo "Before create instance"
#./StrategyCommandLine cmd create_instance TestOneGroup4Strat Group4Strat UIUC SIM-1001-101 dlariviere 1000000 -symbols "DIA|UNH|GS|HD|MSFT|CRM|MCD|HON|BA|V|AMGN|CAT|MMM|NKE|AXP|DIS|JPM|JNJ|TRV|AAPL|WMT|PG|IBM|CVX|MRK|DOW|CSCO|KO|VZ|INTC|WBA|KD"
./StrategyCommandLine cmd create_instance TestTwoGroup4Strat Group4Strat UIUC SIM-1001-101 dlariviere 1000000 -symbols "SPY"
echo "After create instance"
echo "Before instance list"
./StrategyCommandLine cmd strategy_instance_list
echo "After instance list"
./StrategyCommandLine cmd quit


