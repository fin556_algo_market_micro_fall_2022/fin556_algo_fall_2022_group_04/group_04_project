#pragma once

#ifndef _STRATEGY_STUDIO_LIB_EXAMPLES_SIMPLE_MOMENTUM_STRATEGY_H_
#define _STRATEGY_STUDIO_LIB_EXAMPLES_SIMPLE_MOMENTUM_STRATEGY_H_

#ifdef _WIN32
#define _STRATEGY_EXPORTS __declspec(dllexport)
#else
#ifndef _STRATEGY_EXPORTS
#define _STRATEGY_EXPORTS
#endif
#endif

#include <Strategy.h>
#include <MarketModels/Instrument.h>

#include <string>
#include <unordered_map>
#include <iostream>
#include <algorithm> 

#include "kalman_filter.h"

using namespace std;
using namespace RCM::StrategyStudio;
using std::string;
using std::unordered_map;
using std::max;
using std::min;
using std::vector;

class Group4Strat : public Strategy {
public:
    Group4Strat(StrategyID strategyID,
        const std::string& strategyName,
        const std::string& groupName);
    ~Group4Strat();


public: /* from IEventCallback */
    /**
     * This event triggers whenever a Bar interval completes for an instrument
     */
    virtual void OnBar(const BarEventMsg& msg);


    /**
     * This event triggers whenever a signal trade trend is detected
     */
    virtual void OnTrade(const TradeDataEventMsg& msg);

    /**
     * This function detect completed orders and compute quantityHeld
     */
    void OnOrderUpdate(const OrderUpdateEventMsg& msg);
    /**
     *
     *  Perform additional reset for strategy state
     */
    void OnResetStrategyState();

    /**
     * Notifies strategy for every succesfull change in the value of a strategy parameter.
     *
     * Will be called any time a new parameter value passes validation, including during strategy initialization when default parameter values
     * are set in the call to CreateParam and when any persisted values are loaded. Will also trigger after OnResetStrategyState
     * to remind the strategy of the current parameter values.
     */
    void OnParamChanged(StrategyParam& param);

    // void Reprice(Order* order, float price, bool buy);

public:
    KalmanFilter* kf;
    unsigned long counter;
    bool kalman_initialized;
    Eigen::VectorXd* x0;
    Eigen::VectorXd* y;
    std::vector <int> HMMstates;

private:  // Helper functions specific to this strategy
    void AdjustPosition(const Instrument* instrument, int desired_position);
    void SendOrder(const Instrument* instrument, int trade_size);
    void Reprice(const Order* order, float price, bool buy);
    std::vector <int> read_csv(const std::string& filename);
    boost::unordered_map<const Instrument*, OrderID> m_instrument_order_id_map;
    

private: /* from Strategy */
    virtual void RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate);

    /**
     * Define any params for use by the strategy
     */
    virtual void DefineStrategyParams();

};

extern "C" {

    _STRATEGY_EXPORTS const char* GetType() {
        return "Group4Strat";
    }

    _STRATEGY_EXPORTS IStrategy* CreateStrategy(const char* strategyType,
        unsigned strategyID,
        const char* strategyName,
        const char* groupName) {
        if (strcmp(strategyType, GetType()) == 0) {
            return *(new Group4Strat(strategyID, strategyName, groupName));
        }
        else {
            return NULL;
        }
    }

    // must match an existing user within the system
    _STRATEGY_EXPORTS const char* GetAuthor() {
        return "dlariviere";
    }

    // must match an existing trading group within the system
    _STRATEGY_EXPORTS const char* GetAuthorGroup() {
        return "UIUC";
    }

    // used to ensure the strategy was built against a version of
    // the SDK compatible with the server version
    _STRATEGY_EXPORTS const char* GetReleaseVersion() {
        return Strategy::release_version();
    }
}
#endif

