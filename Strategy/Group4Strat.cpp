#ifdef _WIN32
#include "stdafx.h"
#endif

#include "Group4Strat.h"


// create_instance kf_2d_9 KFStrategy UIUC SIM-1001-101 dlariviere 1000000 -symbols SPY
// start_backtest 2022-04-01 2022-04-01 kf_2d_9 1
// export_cra_file /home/vagrant/Desktop/strategy_studio/backtesting/backtesting-results/BACK_kf_test21_2022-10-29_220918_start_04-05-2022_end_04-05-2022.cra  /home/vagrant/Desktop/strategy_studio/backtesting/backtesting-cra-exports

Group4Strat::Group4Strat(StrategyID strategyID,
    const std::string& strategyName,
    const std::string& groupName) :
    Strategy(strategyID, strategyName, groupName),
    m_instrument_order_id_map() {

    int n = 1; // Number of states
    int m = 1; // Number of measurements

    double dt = 1.0 / 30; // Time step

    Eigen::MatrixXd A(n, n); // System dynamics matrix
    Eigen::MatrixXd C(m, n); // Output matrix
    Eigen::MatrixXd Q(n, n); // Process noise covariance
    Eigen::MatrixXd R(m, m); // Measurement noise covariance
    Eigen::MatrixXd P(n, n); // Estimate error covariance


    // Discrete LTI projectile motion, measuring position only
    A << 1;
    C << 1;

    // Reasonable covariance matrices
    Q << .01;
    R << 1;
    P << 1; // ? questionable

    kf = new KalmanFilter(1, A, C, Q, R, P);
    // kf = new KalmanFilter();
    kalman_initialized = false;
    std::vector <int> HMMstates = Group4Strat::read_csv("/home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/group_04_project/HMMstates.csv");

    y = new Eigen::VectorXd(m);

    counter = 0;
}

Group4Strat::~Group4Strat() {
    free(kf);
}

void Group4Strat::DefineStrategyParams() {
    kf->init();
}

void Group4Strat::RegisterForStrategyEvents(StrategyEventRegister* eventRegister, DateType currDate) {
    std::cout << "Registering for the strategy event" << std::endl;
}

void Group4Strat::OnTrade(const TradeDataEventMsg& msg) {
    if (kalman_initialized) {
        *y << msg.trade().price();
        kf->update(*y);
        if (counter > 100) {
            Eigen::VectorXd current_state = kf->state();
            const Instrument* instrument = &msg.instrument();
            float best_bid = instrument->top_quote().bid(); // could consider instrument ->top_quote().bid() or top_quote().ask() to use if they work
            float best_ask = instrument->top_quote().ask();
            int state = HMMstates.at(counter);
            // if the next state is greater than the current price, price is going to rise, buy!

            //messages to check
            //std::cout << "\tBest Bid: " << best_bid << std::endl;
            //std::cout << "\tBest Ask: " << best_ask << std::endl;
            //std::cout << "\tKalman Mean: " << current_state[0] << std::endl;
            //std::cout << "\tCounter: " << counter << std::endl;


            if (current_state[0] > best_ask && portfolio().position(&msg.instrument()) < 500 && state == 0) { // if kalman mean is larger than the best price we can buy at, then we should buy, and that we dont already have a position of 500, we don't want to accumulate a large position
                this->SendOrder(&msg.instrument(), 100);
            }
            else if (current_state[0] < best_bid && portfolio().position(&msg.instrument()) < -500 && state == 0) {//if kalman mean is less than the best price we can sell at, then we should sell
                this->SendOrder(&msg.instrument(), -100);
            }
            else {
                this->AdjustPosition(&msg.instrument(), 0);

            }
        }
    }
    else {
        x0 = new Eigen::VectorXd(1);
        *x0 << msg.trade().price();
        kf->init(0, *x0);
        kalman_initialized = true;
    }
    counter++;
}


void Group4Strat::OnOrderUpdate(const OrderUpdateEventMsg& msg) {
    if (msg.completes_order())
        m_instrument_order_id_map[msg.order().instrument()] = 0;
}

void Group4Strat::OnBar(const BarEventMsg& msg) {
}

void Group4Strat::AdjustPosition(const Instrument* instrument, int desired_position) {
    // we can utilize this function to potentially zero out the portfolio to make sure we have a neutral position when kalman mean is equal or close to equal to the curr_price, do this by utilzing portfolio().position(instrument) to send a order to zero the position.
    int trade_size = desired_position - portfolio().position(instrument);

    if (trade_size != 0) {
        OrderID order_id = m_instrument_order_id_map[instrument];
        //if we're not working an order for the instrument already, place a new order
        if (order_id == 0) {
            SendOrder(instrument, trade_size);
        }
        else {
            //otherwise find the order and cancel it if we're now trying to trade in the other direction
            const Order* order = orders().find_working(order_id);
            trade_actions()->SendCancelOrder(order_id);
            SendOrder(instrument, trade_size);

        }
    }

}

void Group4Strat::Reprice(const Order* order, float price, bool buy)
{
    OrderParams params = order->params();
    params.price = price; //change the price
    params.order_side = (buy) ? ORDER_SIDE_BUY : ORDER_SIDE_SELL;
    trade_actions()->SendCancelReplaceOrder(order->order_id(), params);
}

void Group4Strat::SendOrder(const Instrument* instrument, int trade_size) {
    double price;
    if (trade_size > 0) {
        price = instrument->top_quote().ask();
    }
    else {
        price = instrument->top_quote().bid();
    }
    OrderID order_id = m_instrument_order_id_map[instrument];
    if (order_id == 0) { //Check to see if there is an open order for the instrument, if there isn't send a new order
        OrderParams params(*instrument,
            abs(trade_size),
            price,
            MARKET_CENTER_ID_IEX,
            (trade_size > 0) ? ORDER_SIDE_BUY : ORDER_SIDE_SELL,
            ORDER_TIF_DAY,
            ORDER_TYPE_LIMIT);
        std::cout << "SendTradeOrder(): about to send new order for "
            << trade_size
            << " at $"
            << price
            << std::endl;
        TradeActionResult tra = trade_actions()->SendNewOrder(params);
        if (tra == TRADE_ACTION_RESULT_SUCCESSFUL) {
            std::cout << "Sending new trade order successful!" << std::endl;
            m_instrument_order_id_map[instrument] = params.order_id;
        }
        else {
            std::cout << "Error sending new trade order..." << tra << std::endl;
        }
    }
    else { //if there is an existing order cancel and reprice the order
        const Order* order = orders().find_working(order_id);
        Reprice(order, price, (trade_size > 0) ? true : false);
    }
}

void Group4Strat::OnResetStrategyState() {
    m_instrument_order_id_map.clear();
}

std::vector <int> Group4Strat::read_csv(const std::string& filename)
{
    std::vector <int> result;

    std::ifstream f(filename);
    if (!f) throw std::runtime_error("read_csv(): Failure to open file " + filename);

    std::string s;
    while (std::getline(f, s,','))
    {
        result.push_back(std::stoi(s));
    }

    return result;
}

void Group4Strat::OnParamChanged(StrategyParam& param) {
}

