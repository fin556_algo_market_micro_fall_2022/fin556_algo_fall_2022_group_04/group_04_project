from pykalman import KalmanFilter
from datetime import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import glob


def parser() -> argparse.ArgumentParser:
    """
    Constructs a parser to read in command line arguments.
    
    :return: An args parser.
    """
    parser = argparse.ArgumentParser(description='Command line parser for program',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p','--path',dest='path',type=str,default=r"Trades\*.csv",required=False,
        help='Path to the CSV directory')
    parser.add_argument('-b','--bin',dest='bin_type',type=str,default="seconds",required=False,
        choices=['seconds','trades'],
        help='Method for trades binning; can be either "seconds" or number of "trades"')
    parser.add_argument('-n','--number',dest='bin_amount',type=int,default=60,required=False,
        help='The number of seconds or trades to bin for the respective method')

    args = parser.parse_args()
    parser.print_usage()
    print()
    return args


def create_kalman_filter(trans_mats=1, obs_mats=1, init_state_mean=0, init_state_cov=1, obs_cov=1, trans_cov=0.01) -> KalmanFilter:
    """
    Constructs a Kalman Filter.
    
    :param trans_mats: The value for At. It is a random walk.
    :param obs_mat: The value for Ht.
    :param init_state_mean: Any initial value. It will converge to the true state value.
    :param init_state_cov: Sigma value for the Qt in Equation (1) the Gaussian distribution
    :param obs_cov: Sigma value for the Rt in Equation (2) the Gaussian distribution
    :param trans_cov: A small turbulence in the random walk parameter 1.0
    :return: A Kalman Filter.
    """
    kf = KalmanFilter(
        transition_matrices=[trans_mats],
        observation_matrices=[obs_mats],
        initial_state_mean=init_state_mean,
        initial_state_covariance=init_state_cov,
        observation_covariance=obs_cov,
        transition_covariance=trans_cov)
    return kf


def read_csv_data(directory) -> pd.DataFrame:
    """
    Reads in the CSV file data from a directory.
    
    :param directory: The path to the CSV directory.
    :return: The CSV as a pandas dataframe.
    """
    df_list = []
    for fname in glob.glob(directory):
        df_list.append(pd.read_csv(fname, header=0))
    return pd.concat(df_list, ignore_index=True)


def binning(df, method, number) -> pd.DataFrame:
    """
    Averages the trading prices for a given method over a set amount.

    :param df: Dataframe for calculating trade prices.
    :param method: The method for averaging the trades. 
    :param number: Number of times before updating trading price.
    :return: A list of the binned prices.
    """
    prices = df["PRICE"].to_numpy()
    
    def trade_binning(prices, number) -> list:
        """
        Averages the trading prices based on number of trades.

        :param price: Dataframe for calculating average trade prices.
        :param number: Number of trades before updating trading price.
        :return: A list of the binned prices.
        """
        if number < 1:
            raise Exception("Must bin by at least 1 trade")
        elif number > len(prices):
            raise Exception("Must choose smaller binning number")

        binned_prices = []
        count = 0
        summed_price = 0

        for price in prices:
            summed_price += price
            count += 1
            if count == number:
                binned_prices.append(summed_price/count)
                summed_price, count = 0, 0
        if summed_price != 0 and count != 0:
            binned_prices.append(summed_price/count)

        return binned_prices


    def time_binning(prices, times, number) -> list:
        """
        Averages the trading prices based on amount of time.

        :param prices: Dataframe for calculating trade prices.
        :param times: Dataframe for finding time prices.
        :param number: Number of seconds before updating trading price.
        :return: A list of the binned prices.
        """
        if number < 1:
            raise Exception("Must bin by at least 1 second")
        elif number > 3600:
            raise Exception("Must bin at max once per hour")

        binned_prices = []
        time_difference = 0
        trade_count = 1
        summed_price = prices[0]

        for i in range(1,len(times)):
            last_ts = (datetime.strptime(times[i-1][:-3], '%Y-%m-%d %H:%M:%S.%f')).timestamp()
            curr_ts = (datetime.strptime(times[i][:-3], '%Y-%m-%d %H:%M:%S.%f')).timestamp()
            diff_ts = curr_ts - last_ts
            time_difference += diff_ts

            # 4 hours difference, accounting for after-market to pre-market
            if time_difference >= number or time_difference > 14400:
                binned_prices.append(float(summed_price/trade_count))
                summed_price, trade_count = 0, 0
                time_difference = 0

            summed_price += prices[i]
            trade_count += 1
            

        if summed_price != 0 and trade_count != 0:
            binned_prices.append(float(summed_price/trade_count))

        return binned_prices


    if method == 'trades':
        prices = trade_binning(prices, number)
    elif method == 'seconds':
        # times = df.COLLECTION_TIME.str[11:].to_numpy()
        times = df["COLLECTION_TIME"].to_numpy()
        prices = time_binning(prices, times, number)
    return pd.DataFrame(prices,columns=['Binned_Prices'])


def smooth_kalman(kf, df) -> None:
    """
    Smooths the Kalman filter according to the data.
    
    :param kf: The Kalman Filter.
    :param df: The dataframe of the CSV.
    """
    state_means , _ = kf.filter(df["Binned_Prices"].values)
    df["KF_mean"] = np.array(state_means)
    df.head()


def plot(df, symbol, args) -> None:
    """
    Plots the filter according to the input.

    :param df: The filtered dataframe.
    :param symbol: The trading symbol. 
    :param args: Passing in the arguments parser.
    """
    df[["Binned_Prices", "KF_mean"]].plot()
    plt.title('Kalman Filter Estimates for '+symbol)
    plt.legend([symbol,'Kalman Estimate'])
    if args.bin_type == 'trades':
        plt.xlabel('Trade Bins every '+str(args.bin_amount)+' Trades')
        plt.ylabel('Average Price of Trade Bins every '+str(args.bin_amount)+' Trades')
    elif args.bin_type == 'seconds':
        plt.xlabel('Trade Bins every '+str(args.bin_amount)+' Seconds')
        plt.ylabel('Average Price of Trade Bins every '+str(args.bin_amount)+' Seconds')
    plt.show()


def main():
    """
    Runs program.

    :param argv: Set of arguments for parsing through file.
    """
    args = parser()
    
    kf = create_kalman_filter()
    df = read_csv_data(args.path)
    trade_symbol = df["SYMBOL"][0]
    df = binning(df, args.bin_type, args.bin_amount)
    smooth_kalman(kf, df)
    plot(df, trade_symbol, args)


if __name__ == "__main__":
    main()
