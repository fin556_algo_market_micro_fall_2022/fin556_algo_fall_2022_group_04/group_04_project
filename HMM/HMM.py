# -*- coding: utf-8 -*-

from __future__ import print_function
from datetime import datetime
from hmmlearn.hmm import GaussianHMM
from matplotlib import cm, pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator
import numpy as np
import pandas as pd
import seaborn as sns
import warnings
import pickle
import argparse
import glob


def parser() -> argparse.ArgumentParser:
    """
    Constructs a parser to read in command line arguments.
    
    :return: An args parser.
    """
    parser = argparse.ArgumentParser(description='Command line parser for program',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p','--path',dest='path',type=str,default=r"Trades\*.csv",required=False,
        help='Path to the CSV directory')
    parser.add_argument('-op','--output_pickle',dest='pickle',type=str,default="hmm_model_spy.pkl",required=False,
        help='Output file for the CSV directory')
    parser.add_argument('-b','--bin',dest='bin_type',type=str,default="seconds",required=False,
        choices=['seconds','trades'],
        help='Method for trades binning; can be either "seconds" or number of "trades"')
    parser.add_argument('-n','--number',dest='bin_amount',type=int,default=60,required=False,
        help='The number of seconds or trades to bin for the respective method')
    parser.add_argument('-s','--scaling',dest='scaling_type',type=str,default="z-score",required=False,
        choices=['max-abs','min-max','z-score','None'],
        help='The method for normalizing the data. Can be scaled by max-absolute, min-max feature, or by z-score (recommended for state-determinism). User may also choose to ignore normalizing (NOT RECOMMENDED)')

    args = parser.parse_args()
    parser.print_usage()
    print()
    return args


def read_csv_data(directory) -> pd.DataFrame:
    """
    Reads in the CSV file data from a directory.
    
    :param directory: The path to the CSV directory.
    :return: The CSV as a pandas dataframe.
    """
    df_list = []
    for fname in glob.glob(directory):
        df_list.append(pd.read_csv(fname, header=0))
    return pd.concat(df_list, ignore_index=True)


def binning(df, method, number) -> pd.DataFrame:
    """
    Averages the trading prices for a given method over a set amount.

    :param df: Dataframe for calculating trade prices.
    :param method: The method for averaging the trades. 
    :param number: Number of times before updating trading price.
    :return: A list of the binned prices.
    """
    prices = df["PRICE"].to_numpy()
    
    def trade_binning(prices, number) -> list:
        """
        Averages the trading prices based on number of trades.

        :param price: Dataframe for calculating average trade prices.
        :param number: Number of trades before updating trading price.
        :return: A list of the binned prices.
        """
        if number < 1:
            raise Exception("Must bin by at least 1 trade")
        elif number > len(prices):
            raise Exception("Must choose smaller binning number")

        binned_prices = []
        count = 0
        summed_price = 0

        for price in prices:
            summed_price += price
            count += 1
            if count == number:
                binned_prices.append(summed_price/count)
                summed_price, count = 0, 0

        if summed_price != 0 and count != 0:
            binned_prices.append(summed_price/count)

        return binned_prices


    def time_binning(prices, times, number) -> list:
        """
        Averages the trading prices based on amount of time.

        :param prices: Dataframe for calculating trade prices.
        :param times: Dataframe for finding time prices.
        :param number: Number of seconds before updating trading price.
        :return: A list of the binned prices.
        """
        if number < 1:
            raise Exception("Must bin by at least 1 second")
        elif number > 3600:
            raise Exception("Must bin at least once per hour")

        binned_prices = []
        time_difference = 0
        trade_count = 1
        summed_price = prices[0]

        for i in range(1,len(times)):
            last_ts = (datetime.strptime(times[i-1][:-3], '%Y-%m-%d %H:%M:%S.%f')).timestamp()
            curr_ts = (datetime.strptime(times[i][:-3], '%Y-%m-%d %H:%M:%S.%f')).timestamp()
            diff_ts = curr_ts - last_ts
            time_difference += diff_ts

            # 4 hours difference, accounting for after-market to pre-market
            if time_difference >= number or time_difference > 14400:
                binned_prices.append(float(summed_price/trade_count))
                summed_price, trade_count = 0, 0
                time_difference = 0

            summed_price += prices[i]
            trade_count += 1

        if summed_price != 0 and trade_count != 0:
            binned_prices.append(float(summed_price/trade_count))

        return binned_prices


    if method == 'trades':
        prices = trade_binning(prices, number)
    elif method == 'seconds':
        # times = df.COLLECTION_TIME.str[11:].to_numpy()
        times = df["COLLECTION_TIME"].to_numpy()
        prices = time_binning(prices, times, number)
    return pd.DataFrame(prices,columns=["Binned_Prices"])


def normalize(df, method) -> pd.DataFrame:
    """
    Applies a normalization to the trade data for consistency in the states.
    Once normalized, will also take a natural log of the returns (ln(pct.change)).

    :param df: Dataframe for normalizing trade prices.
    :param method: The input method for normalizing. 
        Can be maximum-absolute scaling, min-max feature scaling, or z-score scaling.
        User can also ignore normalization (not recommended). 
    :return: A dataframe of the normalized prices according to the input method.
    """
    df["Normalized"] = df["Binned_Prices"]
    if method == 'max-abs':
        df["Normalized"] = df["Normalized"] / df["Normalized"].abs().max()
    elif method == 'max-min':
        df["Normalized"] = (df["Normalized"] - df["Normalized"].min()) / (df["Normalized"].max() - df["Binned_Prices"].min())
    elif method == 'z-score':
        df["Normalized"] = (df["Normalized"] - df["Normalized"].mean()) / df["Normalized"].std()        
    
    df["Normalized"] = df["Normalized"].pct_change()
    df["Normalized"] = np.log(df["Normalized"]+1)
    df["Normalized"] = df["Normalized"].fillna(0)
    
    return df


def obtain_column(df) -> np.column_stack:
    """
    Obtain the normalized prices and convert them to a column stack.

    :param df: The dataframe used for trade prices.
    :return: A prices column stack.
    """
    return np.column_stack([df["Normalized"]])
    

def hidden_markov_model(column_stack, component_number = 2, cov_type="full", iter_number=1000) -> tuple[GaussianHMM, list, list]:
    """
    Create the Gaussian Hidden markov Model and fit it to the SPY trades data.

    :param column_stack: Column_stack of all the normalized trade prices.
    :param component_number: Number of components for the HMM.
    :param cov_type: Choosing the covariance type.
    :param iter_number: Number of iterations for the HMM.
    :return: A tuple of the model, a list of the state decisions, and a list of the state probabilities.
    """
    hmm_model = GaussianHMM(
        n_components=component_number,
        covariance_type=cov_type, 
        n_iter=iter_number
    ).fit(column_stack)

    print("Model Score:", hmm_model.score(column_stack))
  
    state_decisions = hmm_model.predict(column_stack)
    state_probabilities = hmm_model.predict_proba(column_stack)

    return hmm_model, state_decisions, state_probabilities


def to_pickle(hmm, pickle_path) -> None:
    """
    A function that output the HMM to a pickle file. Will save this file to the provided path.

    :param hmm: The model for saving.
    :param pickle_path: The path to the directory where the .pkl file should be saved.
    """
    print("Pickling HMM model...")
    pickle.dump(hmm, open(pickle_path, "wb"))
    print("...HMM model pickled.")


def plot(df, state_decisions, symbol, args):
    """
    Plots the filter according to the input.

    :param df: The filtered dataframe.
    :param state_decisions: The list of the state decisions for plotting.
    """
    fig, axs = plt.subplots(
        2, 
        sharex=True, sharey=True
    )
    colours = cm.rainbow(
        np.linspace(0, 1,2)
    )
    for i, (ax, colour) in enumerate(zip(axs, colours)):
        mask = state_decisions == i
        ax.plot(
            df.index[mask], 
            df[["Binned_Prices"]][mask], 
            ".", linestyle='none', 
            c=colour
        )
        ax.set_title("Hidden State #%s" % i)
        ax.grid(True)   
    if args.bin_type == 'trades':
        fig.supxlabel('Trade Bins every '+str(args.bin_amount)+' Trades')
        fig.supylabel('Average Price of Trade Bins every '+str(args.bin_amount)+' Trades')
    elif args.bin_type == 'seconds':
        fig.supxlabel('Trade Bins every '+str(args.bin_amount)+' Seconds')
        fig.supylabel('Average Price of Trade Bins every '+str(args.bin_amount)+' Seconds')
    fig.suptitle('HMM State model for '+symbol)
    plt.show()


def main():
    """
    Runs program.
    """
    # #Hides deprecation warnings for sklearn
    # warnings.filterwarnings("ignore")

    args = parser()
    df = read_csv_data(args.path)
    trade_symbol = df["SYMBOL"][0]
    df = binning(df, args.bin_type, args.bin_amount)

    df = normalize(df, args.scaling_type)

    column_stack = obtain_column(df)
    
    hmm_model, state_decisions, state_probabilities = hidden_markov_model(column_stack)
    to_pickle(hmm_model, args.pickle)
    state_decisions.tofile('HMMStates.csv', sep = ',')
    plot(df, state_decisions, trade_symbol, args)


if __name__ == "__main__":
    main()
